@extends('layouts.nav')
@section('title','Edit Data')
@section('content')
    {{-- <a></a> --}}
    <form action={{ route('store') }} method="post">
        @csrf
        <div class="form-group">
            <label for="Id">Id</label>
            <input class = "form-control" name ='id' type="text" value="{{ $employee->id}}" disabled>
        </div>
        <div class="form-group">
            <label for="Nama">Nama</label>
            <input class="form-control" id="nama" name="nama" placeholder="Nama" value="{{ $employee->nama }}">
        </div>
        <div class="form-group">
            <label for="IDAtasan">ID Atasan</label>
            <select class="form-control" id="atasan_id" name="atasan_id">
                @foreach($atasan as $atasan)
                {{-- <?php if $atasan->id == $employee -> atasan_id then echo checked?> --}}
                    <option value='{{ $atasan->id }}' >{{ $atasan -> id }}</option>
                @endforeach
                {{-- <option value>1</option> --}}
            </select>
            
        </div>
        <div class="form-group">
            <label for="IDCOmpany">ID Company</label>
            <select class="form-control" id="company_id" name="company_id">
                @foreach($company as $comp)
                    <option value="{{ $comp->id}}">{{ $comp -> id }}</option>
                @endforeach
                    {{-- <option>1</option> --}}
            </select>
        </div>
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection