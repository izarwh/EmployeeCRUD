<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Employeecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // mengambil data
        $data = DB::table('employee')->get();
        return view('app.indexview',['emp'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Input member
        $atasan = DB::select("SELECT id from employee");
        $company = DB::select("SELECT id from company");
        return view('app.createview', compact('atasan','company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::table('employee')->insert([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ]);

        return redirect('/employee/tambah');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = DB::table('employee')->where('id',$id)->first();
        $atasan = DB::select("SELECT id from employee");
        $company = DB::select("SELECT id from company");
        // echo $employee;
        return view('app.editview',compact('employee','atasan','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //memasukkan data ke database
        DB::table('employee')->where('id',$id)->update([
            'nama' =>$request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ]);

        return redirect('/employee/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //menghapus data ke database
        DB::table('employee')->where('id',$id)->delete();

        return back();
    }
}
