<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Employeecontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/employee/list',[Employeecontroller::class,'index']) -> name('index');
Route::get('/employee/tambah',[Employeecontroller::class,'create']) ->name('tambah');
Route::post('/employee/store',[Employeecontroller::class,'store']) ->name('store');
Route::get('/employee/edit/{id}',[Employeecontroller::class,'edit']) ->name('edit');
Route::post('/employee/update/{id}',[Employeecontroller::class,'update']) ->name('update');
Route::get('/employee/delete/{id}',[Employeecontroller::class,'destroy']) ->name('delete');